var os = require('os');
var fs = require('fs');
var path = require('path');
var snmp = require('snmpjs');
var logger = require('bunyan');
var freeport = require('freeport');
var csv = require('csv-load-sync');
var fs = require('fs');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Stream = require('stream');
var mysql = require('mysql');

var mysql_connection = mysql.createConnection({
  host: '54.187.24.6',
  user: 'simulator',
  password: 's!mulat0r',
  database: 'simulator'
});

var stream = new Stream();
stream.writable = true
stream.write = function(obj) {
    //console.log(obj);
    if (typeof obj == 'string' || obj instanceof String){
        io.emit('log', obj);
    }else if(obj.port && obj.oid){
        io.emit('log', "GET: " + obj.port + " : " + obj.oid + " : " + obj.value);    
        var rec = {
            port : obj.port,
            oid: obj.oid,
            value: "" + obj.value,
            modifiedtime: new Date() 
        }
        var qry = mysql_connection.query('INSERT INTO snmpsimdata  SET ?', rec, function (err, result) {
            if(err){
                console.log(err);               
            }else{
                console.log(result);
            }
        });   
        console.log(qry.sql);
    }
}

var log = new logger({
    name: 'snmpd',
    streams: [{
        level: 'debug',
        type: "raw",
        stream: stream
     }],    
});

var CONFIG_FOLDER = path.join(__dirname, "config");
var DATA_FOLDER = path.join(CONFIG_FOLDER, "data");
var DRIVER_FOLDER = path.join(CONFIG_FOLDER, "driver");

var indexData = JSON.parse(fs.readFileSync(CONFIG_FOLDER + '/index.json', 'utf8'));
indexData.forEach(function (entry, index) {
    entry.instance = [];
    for (var i = 0; i < entry.count; i++) {
        initInstance(i, entry);
    }
});

function initInstance(index, device) {
    freeport(function (er, port) {
        device.instance[index] = {
            port: port,
            instance: index
        }
        initAgent(device, device.instance[index]);
    });
}

function initAgent(device, instance) {

    var data = csv(DATA_FOLDER + "/" + device.name + ".csv");
    var driver = {
        oids: {}
    };
    if (fs.existsSync(DRIVER_FOLDER + "/" + device.name + ".js")) {
        var DriverModule = require(DRIVER_FOLDER + "/" + device.name + ".js");
        driver = new DriverModule();
    }

    var agent = snmp.createAgent({
        log: log
    });
    var oids = [];
    data.forEach(function (record) {
        var oid = {};
        oid.oid = record.oid;
        oid.handler = function (prq) {
            console.log("Hander Invoked > ");
            console.log("\top = " + prq.op);
            console.log("\toid = " + prq.oid);
            console.log("\tvalue = " + prq.value);
            if(prq.op == snmp.pdu.GetRequest || prq.op == snmp.pdu.GetNextRequest ){
                console.log("\t\tIn get/GetNext");
                var val =  record.value;
                if(driver.oids[record.oid]){
                    console.log("\t\tOID defined in Driver");
                    if(driver.oids[record.oid].value){
                        console.log("\t\tvalue method present in driver. Invoking it.");
                        val = driver.oids[record.oid].value() 
                    }
                }
                if(record.type == 'Integer'){
                    val = parseInt(val)
                }
                log.info({oid:prq.oid,value: val, port:instance.port});
                var val = snmp.data.createData({
                    type: record.type,
                    value: val
                });            
                snmp.provider.readOnlyScalar(prq, val);
            }else if(prq.op == snmp.pdu.SetRequest){
                console.log("\t\tIn Set");
                console.log("\t\tdata = " + prq.value.value);                
                if(driver.oids[record.oid] && driver.oids[record.oid].set){                   
                    driver.oids[record.oid].set(prq.value.value) 
                }else{
                    record.value = prq.value.value;
                } 
                prq.done(snmp.pdu.noError);                
            }else{
                console.log("\t\tIn Unimplemented Operation");
                prq.done(snmp.pdu.noSuchName);
            }
            console.log("Hander Completed > ");
        }
        oids.push(oid);
    });
    agent.request(oids);
    agent.bind({
        family: 'udp4',
        port: instance.port
    });

}

app.get('/port', function(req, res){
    res.send(indexData);
});
app.get('/log', function(req, res){
    res.sendfile('log.html');
});
http.listen(3000, function(){});

//TODO types http://joyent.github.io/node-snmpjs/protocol.html
